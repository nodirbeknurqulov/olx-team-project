package uz.pdp.olxteamproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.olxteamproject.common.ApiResponse;
import uz.pdp.olxteamproject.entity.Category;
import uz.pdp.olxteamproject.service.CategoryService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/category")
public class CategoryController {

    public final CategoryService categoryService;

    @PostMapping
    public ResponseEntity<?> addNewCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Integer id) {
        return categoryService.deleteCategoryById(id);
    }

    @GetMapping
    public ResponseEntity<?> showAllCategories() {
        return categoryService.getAllCategory();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable Integer id, @RequestBody Category category) {
        return categoryService.updateCategoryById(category, id);
    }
//
//    @GetMapping("/parent/{parentId}")
//    public ResponseEntity<?> showCategoriesByParentId(@PathVariable Long parentId){
//        ApiResponse category = categoryService.getCategory(parentId);
//        return ResponseEntity.status(category.isSuccess()?200:400).body(category);
//    }

    @GetMapping("/getParentId/{parentId}")
    public HttpEntity<?> getCategoryByParentId(@PathVariable Long parentId){
        ApiResponse categoryByParentId = categoryService.getCategoryByParentId(parentId);
        return ResponseEntity.status(categoryByParentId.isSuccess() ? 200 : 404).body(categoryByParentId);
    }

}
