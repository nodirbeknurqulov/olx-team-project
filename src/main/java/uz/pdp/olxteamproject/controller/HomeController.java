package uz.pdp.olxteamproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.olxteamproject.service.HomeService;

// Nurkulov Nodirbek 4/13/2022  6:50 PM
@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class HomeController {

    private final HomeService homeService;

    @GetMapping
    public String getAllActorsByPageable() {
       return "Hello, our team!!";
    }
}
