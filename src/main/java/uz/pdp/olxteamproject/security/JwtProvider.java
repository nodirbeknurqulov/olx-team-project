package uz.pdp.olxteamproject.security;

import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

import java.util.Date;

// Nurkulov Nodirbek 4/8/2022  9:58 AM
@Component
public class JwtProvider {

    static long expireTime=36_000_000_000L;
    static String key = "secretKey";

    public String generateToken(String username, boolean b) {

        String generatedToken = Jwts
                .builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireTime))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        return generatedToken;
    }

    public boolean validateToken(String token) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException e) {
            System.out.println("tokenni muddati otgan");
        } catch (MalformedJwtException malformedJwtException) {
            System.out.println("buzilgan token");
        } catch (SignatureException signatureException) {
            System.out.println("key xato");
        } catch (UnsupportedJwtException unsupportedJwtException) {
            System.out.println("Qo'llanilmagan token");
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("bosh token");
        }
        return false;
    }

    public String getUsernameFromToken(String token) {
        String usernameFromToken = Jwts
                .parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return usernameFromToken;
    }
}