package uz.pdp.olxteamproject.entity;

import lombok.*;
import uz.pdp.olxteamproject.entity.template.AbsEntity;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {

    byte[] data;

    @OneToOne(cascade = CascadeType.MERGE)
    private Attachment attachment;
}