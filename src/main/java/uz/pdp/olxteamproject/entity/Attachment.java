package uz.pdp.olxteamproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import uz.pdp.olxteamproject.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "attachments")
public class Attachment extends AbsEntity {

    private String originalFileName;

    private String name;

    private long size;

    private String contentType;

    @JsonIgnore
    @OneToOne(mappedBy = "attachment",cascade = CascadeType.MERGE)
    private AttachmentContent attachmentContent;

    public Attachment(String name, long size, String contentType,String originalFileName) {
        this.name = name;
        this.size = size;
        this.contentType = contentType;
        this.originalFileName=originalFileName;
    }
}
