package uz.pdp.olxteamproject.entity;

import lombok.*;
import uz.pdp.olxteamproject.entity.template.AbsEntity;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "tariffs")
public class Tariff extends AbsEntity {

    private String name;

    private int days;

//    public Tariff(String name, int days) {
//        this.name = name;
//        this.days = days;
//    }
}