package uz.pdp.olxteamproject.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "regions")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "region", cascade = CascadeType.MERGE)
    private List<District> districtList;

    public Region(String name) {
        this.name = name;
    }
}

