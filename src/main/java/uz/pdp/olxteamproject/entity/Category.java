
package uz.pdp.olxteamproject.entity;

import lombok.*;
import uz.pdp.olxteamproject.entity.template.AbsEntity;

import javax.persistence.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "categories")
public class Category extends AbsEntity {

    private String name;

    private boolean isEnable;

    private Integer parentId;
}
