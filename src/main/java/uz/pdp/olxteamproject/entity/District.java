package uz.pdp.olxteamproject.entity;

import lombok.*;
import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "districts")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Region region;

    public District(String name, Region region) {
        this.name = name;
        this.region = region;
    }
}
