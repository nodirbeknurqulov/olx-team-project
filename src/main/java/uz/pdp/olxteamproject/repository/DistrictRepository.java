package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.olxteamproject.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {

}
