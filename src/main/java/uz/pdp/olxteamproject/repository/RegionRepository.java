package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.olxteamproject.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Integer> {

}
