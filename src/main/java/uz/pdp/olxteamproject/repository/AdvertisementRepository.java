package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.olxteamproject.entity.Advertisement;

public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer> {

}
