package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.olxteamproject.entity.Tariff;

public interface TariffRepository extends JpaRepository<Tariff,Integer> {

}

