package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.olxteamproject.entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    @Query(nativeQuery = true,value = "select c.name, c.parent_id\n" +
            "       from categories c where c.parent_id=:parentId;")
//    List<Category>  getByParentId(Long parentId);

    List<Category> findCategoryByParentId(Long parentId);
}
