package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.olxteamproject.entity.Home;

public interface HomeRepository extends JpaRepository<Home,Integer> {
}
