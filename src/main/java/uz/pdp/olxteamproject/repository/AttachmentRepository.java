package uz.pdp.olxteamproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.olxteamproject.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {


}
