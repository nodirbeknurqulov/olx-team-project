package uz.pdp.olxteamproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.olxteamproject.common.ApiResponse;
import uz.pdp.olxteamproject.entity.Category;
import uz.pdp.olxteamproject.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public ResponseEntity<?> addCategory(Category newCategory) {
        List<Category> categoryList = categoryRepository.findAll();
        for (Category category : categoryList) {
            if (category.getName().equals(newCategory.getName())) {
                return new ResponseEntity<>("Category with this name already exists!", HttpStatus.CONFLICT);
            }
        }
        categoryRepository.save(newCategory);
        return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
    }

    public ResponseEntity<?> getAllCategory() {
        List<Category> categories = categoryRepository.findAll();
        if (categories.isEmpty()) {
            return new ResponseEntity<>("Categories not found!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(categories, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> deleteCategoryById(Integer id) {

        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (!categoryOptional.isPresent()) {
            return new ResponseEntity<>("Not deleted!", HttpStatus.NOT_FOUND);
        }
        categoryRepository.deleteById(id);
        return new ResponseEntity<>("Successfully deleted", HttpStatus.NO_CONTENT);

    }

    public ResponseEntity<?> updateCategoryById(Category category, Integer id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (!optionalCategory.isPresent()) {
            return new ResponseEntity<>("Category not found!", HttpStatus.NOT_FOUND);
        }
        optionalCategory.get().setName(category.getName());
        optionalCategory.get().setParentId(category.getParentId());
        categoryRepository.save(optionalCategory.get());
        return new ResponseEntity<>("Successfully updated!", HttpStatus.OK);
    }

//    public ApiResponse getCategory(Long parentId){
//         List<Category> categoryList = categoryRepository.getByParentId(parentId);
//         if (categoryList.size()==0)
//             return new ApiResponse("NOT FOUND",false);
//         return new ApiResponse("SUCCESS", true,categoryList);

    public ApiResponse getCategoryByParentId(Long parentId) {
        List<Category> categoryByParentId = categoryRepository.findCategoryByParentId(parentId);
        if (categoryByParentId.isEmpty()) {
            return new ApiResponse("Category not found!", false);
        }
        return new ApiResponse("Success!", true, categoryByParentId);
    }



}




