package uz.pdp.olxteamproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.olxteamproject.repository.UserRepository;

// Nurkulov Nodirbek 4/8/2022  10:29 AM
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Optional<User> optionalUser = userRepository.findByUsername(username);
//        if (!optionalUser.isPresent()) {
//            throw new UsernameNotFoundException(username);
//        }
//        User user = optionalUser.get();
//        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(), Collections.emptyList());
//    }
}
