package uz.pdp.olxteamproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.olxteamproject.entity.User;
import uz.pdp.olxteamproject.payload.ApiResponse;
import uz.pdp.olxteamproject.payload.LoginDto;
import uz.pdp.olxteamproject.payload.RegisterDto;
import uz.pdp.olxteamproject.repository.RoleRepository;
import uz.pdp.olxteamproject.repository.UserRepository;
import uz.pdp.olxteamproject.security.JwtProvider;

import java.util.Collections;

// Nurkulov Nodirbek 4/14/2022  9:26 AM

@Service
@RequiredArgsConstructor
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final JwtProvider jwtProvider;
    private final AuthenticationManager authenticationManager;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    //REGISTERING USER
    public HttpEntity<?>registerUser(RegisterDto registerDto) {

        if (userRepository.existsByEmail(registerDto.getEmail())) {
            return new ResponseEntity<>(new ApiResponse("Something went wrong!!!", false), HttpStatus.ALREADY_REPORTED);
        }

        User user = new User();
        user.setFullName(registerDto.getFullName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setRole(Collections.singleton(roleRepository.findByName("ROLE_USER")));
        userRepository.save(user);
        return new ResponseEntity<>(new ApiResponse("Successfully registered", true), HttpStatus.OK);
    }

    //LOGIN USER
    public HttpEntity<?> login(LoginDto loginDto) {

        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginDto.getEmail(), loginDto.getPassword()
            ));

            User principal = (User) authentication.getPrincipal();
            String generatedToken = jwtProvider.generateToken(principal.getEmail(), true);
            return new ResponseEntity<>(new ApiResponse("Token", true, generatedToken), HttpStatus.OK);

        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(new ApiResponse("Email or password not found", false), HttpStatus.NOT_FOUND);

        }
    }
}
