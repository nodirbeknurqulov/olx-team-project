package uz.pdp.olxteamproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OlxTeamProjectApplication {
    public static void main(String[] args) {
        SpringApplication.run(OlxTeamProjectApplication.class, args);
    }
}
